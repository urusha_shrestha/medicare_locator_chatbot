import mysql.connector
import traceback

def getData(query:str):
    try:
        mydb = mysql.connector.connect(
            host="localhost",
            user ="root",
            password="root",
            database="medicare_details"
        )
        cursor = mydb.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        return results
    except:
        print("Error occured while connecting to dataase or fetching data from the database. Error Trace: {}".format(traceback.format_exc()))
        return []
#print (getData("SELECT * FROM medicare_details.medicare;"))